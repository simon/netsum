#define DEFAULT_COUNT	10000
#define DEFAULT_SIZE	1500
#define DEFAULT_TIMEOUT	60

#define DIR_TX		(1 << 0)
#define DIR_RX		(1 << 2)
#define USE_MEMCMP	(1 << 2)

struct netsum_ctrl {
	uint32_t count;
	uint32_t size;
	uint32_t timeout;
	uint8_t flags;
	uint64_t rx;
	uint64_t rx_sum;
	double rx_time;
	double rx_time_sum;
	uint64_t tx;
	uint64_t tx_sum;
	double tx_time;
	double tx_time_sum;
};

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
