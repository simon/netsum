/*
 * This file is part of netsum.
 *
 * Copyright (C) 2014 Simon Guinot <simon.guinot@sequanux.org>
 *
 * netsum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * netsum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with netsum.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include "netsum.h"
#include "net.h"
#include "buffer.h"

#ifdef HAVE_GETOPT_LONG
#define _GNU_SOURCE
#include <getopt.h>

static const struct option long_options[] =
{
	{"check", 0, 0, 'C'},
	{"timeout", 1, 0, 'T'},
	{"count", 1, 0, 'c'},
	{"help", 0, 0, 'h'},
	{"memcmp", 0, 0, 'm'},
	{"rx", 0, 0, 'r'},
	{"size", 1, 0, 's'},
	{"tx", 0, 0, 't'},
	{"verbose", 0, 0, 'v'},
	{0, 0, 0, 0}
};
#endif

static const char *short_options = "CT:c:hmrs:tv";

static void usage(void)
{
	fprintf(stdout, "Usage : netsum [OPTIONS] [ADDRESS]\n\n");
#ifdef HAVE_GETOPT_LONG
	fprintf(stdout, "The address format is IP|hosname[:port] (only IPv4 supported).\n");
	fprintf(stdout, "\nOptions:\n");
	fprintf(stdout, "    -h, --help       display this help\n");
	fprintf(stdout, "    -v, --verbose    enable verbose messages\n");
	fprintf(stdout, "    -C, --check      run unit tests\n");
	fprintf(stderr, "    -T, --timeout    socket timeout in secs\n");
	fprintf(stderr, "    -m, --memcmp     offload CRC checks with memcmp\n");
	fprintf(stdout, "\nClient options:\n");
	fprintf(stdout, "    -c, --count      number of messages sent/received\n");
	fprintf(stdout, "    -s, --size       message size in bytes\n");
	fprintf(stdout, "    -r, --rx         enable message Rx testing\n");
	fprintf(stdout, "    -t, --tx         enable message Tx testing\n");
#else
	fprintf(stdout, "    -h    display this help\n");
	fprintf(stdout, "    -v    enable debug traces\n");
	fprintf(stderr, "    -C    run unit tests\n");
	fprintf(stderr, "    -T    socket timeout in secs\n");
	fprintf(stderr, "    -m    offload CRC checks with memcmp\n");
	fprintf(stdout, "\nClient options:\n");
	fprintf(stdout, "    -c    number of messages sent/received\n");
	fprintf(stdout, "    -s    message size in bytes\n");
	fprintf(stdout, "    -r    enable message Rx testing\n");
	fprintf(stdout, "    -t    enable message Tx testing\n");
#endif
}

static bool run_tests = false;
bool verbose;

static struct netsum_ctrl ctrl = {
	.count		= DEFAULT_COUNT,
	.size		= DEFAULT_SIZE,
	.timeout	= DEFAULT_TIMEOUT,
};

int main(int argc, char *argv[])
{
	int opt;
#ifdef HAVE_GETOPT_LONG
	int option_index = 0;

	while ((opt = getopt_long(argc, argv, short_options, long_options,
					&option_index)) != EOF)
#else
	while ((opt = getopt(argc, argv, short_options)) != EOF)
#endif
	{
		switch (opt) {
		case 'C': /* --check */
			run_tests = true;
			break;
		case 'T': /* --timeout */
			ctrl.timeout = atoi(optarg);
			break;
		case 'c': /* --count */
			ctrl.count = atoll(optarg);
			break;
		case 'm': /* --memcmp */
			ctrl.flags |= USE_MEMCMP;
			break;
		case 'h': /* --help */
			usage();
			return 0;
		case 'r': /* --rx */
			ctrl.flags |= DIR_RX;
			break;
		case 's': /* --size */
			ctrl.size = atoi(optarg);
			break;
		case 't': /* --tx */
			ctrl.flags |= DIR_TX;
			break;
		case 'v': /* --verbose */
			verbose = true;
			break;
		default:
			return -1;
		}
	}

	srand(time(NULL));

	if (run_tests)
		return run_buffer_tests();

	return run_netsum(argv[optind], &ctrl);
}
