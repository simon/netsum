/*
 * This file is part of netsum.
 *
 * Copyright (C) 2014 Simon Guinot <simon.guinot@sequanux.org>
 *
 * netsum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * netsum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with netsum.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <ifaddrs.h>
#include <inttypes.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "netsum.h"
#include "buffer.h"

#define NETSUM_DEFAULT_PORT 3333

extern bool verbose;

struct netsum_ctrl_msg {
	uint32_t count;
	uint32_t size;
	uint32_t timeout;
	uint8_t dir;
} __attribute__((packed));

/*
 * Connection.
 */

static int get_inet_addr(const char *addr, struct sockaddr_in *sin)
{
	char *tmp, *ip_str, *port_str;
	int ret = -1;
	uint16_t port = NETSUM_DEFAULT_PORT;

	tmp = strdup(addr);
	if (!tmp) {
		fprintf(stderr, "strdup() failed: %s\n", strerror(errno));
		return -1;
	}

	ip_str = tmp;
	port_str = strchr(tmp, ':');
	if (port_str) {
		*port_str = '\0';
		port_str++;
		port = (uint16_t) atoi(port_str);
	}

	if (!inet_aton(ip_str, &sin->sin_addr))
		goto err_free;
	sin->sin_port = htons(port);
	sin->sin_family = AF_INET;

	ret = 0;
err_free:
	free(tmp);
	return ret;
}

static int addr_is_local(struct sockaddr_in *sin)
{
	int ret = 0;
	struct ifaddrs *ifaddr, *ifa;

	if (getifaddrs(&ifaddr) == -1) {
		fprintf(stderr, "getifaddrs() failed: %s\n", strerror(errno));
		return -1;
	}

	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		struct sockaddr_in *local_sin;

		if (ifa->ifa_addr->sa_family != AF_INET)
			continue;

		local_sin = (struct sockaddr_in *) ifa->ifa_addr;
		if (local_sin->sin_addr.s_addr == sin->sin_addr.s_addr) {
			ret = 1;
			break;
		}
	}
	freeifaddrs(ifaddr);

	return ret;
}

static int open_socket(time_t timeout, size_t recvlowat)
{
	int sock;
	int on = 1;
	struct timeval tv = {
		tv.tv_sec = timeout,
		tv.tv_usec = 0,
	};

	sock = socket(PF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		fprintf(stderr, "socket() failed: %s\n", strerror(errno));
		goto err;
	}
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1) {
		fprintf(stderr, "setsockopt() SO_REUSEADDR failed: %s\n",
			strerror(errno));
		goto err_close_sock;
	}
	if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) == -1) {
		fprintf(stderr, "setsockopt() SO_RCVTIMEOUT failed: %s\n",
			strerror(errno));
		goto err_close_sock;
	}
	if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) == -1) {
		fprintf(stderr, "setsockopt() SO_SNDTIMEOUT failed: %s\n",
			strerror(errno));
		goto err_close_sock;
	}

	return sock;

err_close_sock:
	close(sock);
err:
	return -1;
}
	
static int
connect_server(struct sockaddr_in *sin, time_t timeout, size_t recvlowat)
{
	int sock;

	sock = open_socket(timeout, recvlowat);
	if (sock == -1)
		return -1;

	if (connect(sock, (struct sockaddr *) sin, sizeof(*sin)) == -1) {
		fprintf(stderr, "connect() failed: %s\n", strerror(errno));
		close(sock);
		return -1;
	}

	return sock;
}

static int
open_server_sock(struct sockaddr_in *sin, time_t timeout, size_t recvlowat)
{
	int ret;
	int sock;
	socklen_t addrlen = sizeof(*sin);

	sock = open_socket(timeout, recvlowat);
	if (sock == -1)
		return -1;

	ret = bind(sock, (struct sockaddr *) sin, sizeof(*sin));
	if (ret == -1) {
		fprintf(stderr, "bind() failed: %s\n", strerror(errno));
		goto err_sock;
	}
	ret = listen(sock, 1);
	if (ret == -1) {
		fprintf(stderr, "listen() failed: %s\n", strerror(errno));
		goto err_sock;
	}
	ret = getsockname(sock, (struct sockaddr *) sin, &addrlen);
	if (ret == -1) {
		fprintf(stderr, "getsockname() failed: %s\n", strerror(errno));
		goto err_sock;
	}
	fprintf(stdout, "Listen at %s:%d\n",
		inet_ntoa(sin->sin_addr), ntohs(sin->sin_port));

	return sock;

err_sock:
	close(sock);
	return ret;
}

/*
 * Messages.
 */

static int send_message(int sock, unsigned char *msg, int len,
			struct sockaddr *daddr, socklen_t addrlen)
{
	int sent, retry, ret;

	for (sent = 0, retry = 0; sent != len && retry < 3;) {
		ret = sendto(sock, msg, len - sent,
			     MSG_NOSIGNAL, daddr, addrlen);
		if (ret == -1) {
			if (errno == EAGAIN || errno == EINTR) {
				retry++;
				continue;
			}
			break;
		}
		sent += ret;
		msg += ret;
	}
	return sent;
}

static int recv_message(int sock, unsigned char *msg, int len,
			struct sockaddr *saddr, socklen_t *addrlen)
{
	int received, retry, ret;

	for (received = 0, retry = 0; received != len && retry < 3;) {
		ret = recvfrom(sock, msg, len - received,
			       MSG_NOSIGNAL, saddr, addrlen);
		if (ret == -1) {
			if (errno == EAGAIN || errno == EINTR) {
				retry++;
				continue;
			}
			break;
		} else if (ret == 0) {
			break;
		}
		received += ret;
		msg += ret;
	}
	return received;
}

static int send_ctrl_message(int sock, struct netsum_ctrl_msg *ctrl_msg,
			     struct sockaddr *daddr, socklen_t addrlen)
{
	int ret;

	ret = send_message(sock, (unsigned char *) ctrl_msg, sizeof(*ctrl_msg),
			   daddr, addrlen);
	if (ret != sizeof(*ctrl_msg))
		return -1;

	return 0;
}

static int recv_ctrl_message(int sock, struct netsum_ctrl_msg *ctrl_msg,
			     struct sockaddr *saddr, socklen_t *addrlen)
{
	int ret;

	ret = recv_message(sock, (unsigned char *) ctrl_msg, sizeof(*ctrl_msg),
			   saddr, addrlen);
	if (ret != sizeof(*ctrl_msg))
		return -1;

	return 0;
}

static void show_stats(struct netsum_ctrl *ctrl)
{
	int i;
	uint64_t sum;
	bool print;
	char *units[] = { "KB", "MB", "GB", "TB" };
	double time, time_sum;

	fprintf(stdout, "\33[2K\rTx:");
	sum = ctrl->tx_sum >> 10;
	print = false;
	for (i = ARRAY_SIZE(units) - 1; i >= 0; i--) {
		uint64_t val = sum >> (10 * i);

		if (print || val) {
			fprintf(stdout, " %04" PRIu64 "%s", val, units[i]);
			sum -= (val << (10 * i));
			print = true;
		}
	}
	/* Avoid division by zero. */
	time = ctrl->tx_time ? : 1; 
	time_sum = ctrl->tx_time_sum ? : 1; 
	fprintf(stdout, " (%3.3fMB/sec %3.3fMB/sec)",
		((ctrl->tx >> 20) / time),
		((ctrl->tx_sum >> 20) / time_sum));

	fprintf(stdout, " - Rx:");
	sum = ctrl->rx_sum >> 10;
	print = false;
	for (i = ARRAY_SIZE(units) - 1; i >= 0; i--) {
		uint64_t val = sum >> (10 * i);

		if (print || val) {
			fprintf(stdout, " %04" PRIu64 "%s", val, units[i]);
			sum -= (val << (10 * i));
			print = true;
		}
	}
	/* Avoid division by zero. */
	time = ctrl->rx_time ? : 1; 
	time_sum = ctrl->rx_time_sum ? : 1; 
	fprintf(stdout, " (%3.3fMB/sec %3.3fMB/sec)",
		((ctrl->rx >> 20) / time),
		((ctrl->rx_sum >> 20) / time_sum));

	fflush(stdout);
}

static int send_data_messages(int sock, struct netsum_ctrl *ctrl,
			      struct sockaddr *daddr, socklen_t addrlen)
{
	unsigned char *buff;
	uint32_t i;
	int ret;
	struct timeval start_time, stop_time;
	double elapsed;

	/* Prepare payload */
	buff = malloc(ctrl->size);
	if (!buff) {
		fprintf(stderr, "malloc() failed: %s\n", strerror(errno));
		return -1;
	}
	fill_buffer(buff, ctrl->size);

	gettimeofday(&start_time, NULL);

	for (i = 0; i < ctrl->count; i++) {
		ret = send_message(sock, buff, ctrl->size, daddr, addrlen);
		if (ret != ctrl->size) {
			ret = -1;
			goto err_free;
		}
	}

	gettimeofday(&stop_time, NULL);
	elapsed = stop_time.tv_sec - start_time.tv_sec +
		  ((double) (stop_time.tv_usec - start_time.tv_usec) / 1000000);

	ctrl->tx = (uint64_t) ctrl->count * (uint64_t) ctrl->size;
	ctrl->tx_sum += ctrl->tx;
	ctrl->tx_time = elapsed;
	ctrl->tx_time_sum += elapsed;

	show_stats(ctrl);
	ret = 0;

err_free:
	free(buff);
	return ret;
}

static int recv_data_messages(int sock, struct netsum_ctrl *ctrl,
			      struct sockaddr *saddr, socklen_t *addrlen)
{
	unsigned char *buff;
	unsigned char *template = NULL;
	bool use_memcmp = !!(ctrl->flags & USE_MEMCMP);
	int ret = -1;
	struct timeval start_time, stop_time;
	uint32_t i;
	double elapsed;

	buff = malloc(ctrl->size);
	if (!buff) {
		fprintf(stderr, "malloc() failed: %s\n", strerror(errno));
		return -1;
	}

	template = malloc(ctrl->size);
	if (!template) {
		fprintf(stderr, "malloc() failed: %s\n",
			strerror(errno));
		goto err_free;
	}

	gettimeofday(&start_time, NULL);

	for (i = 0; i < ctrl->count; i++) {
		ret = recv_message(sock, buff, ctrl->size, saddr, addrlen);
		if (ret != ctrl->size) {
			ret = -1;
			goto err_free;
		}
		if (use_memcmp) {
			/*
			 * For the first buffer received or on error, fall back
			 * on buffer_is_valid().
			 */
			if (i == 0)
				memcpy(template, buff, ctrl->size);
			else if (!memcmp(buff, template, ctrl->size))
				continue;
		}
		if (!buffer_is_valid(buff, ctrl->size, true)) {
			fprintf(stdout,
				"Reference buffer stored in file: ref\n");
			fprintf(stdout,
				"Corrupted buffer stored in file: err\n");
			write_buffer_to_file("ref", buff, ctrl->size);
			write_buffer_to_file("err", template, ctrl->size);
			ret = -1;
			goto err_free;
		}
	}

	gettimeofday(&stop_time, NULL);
	elapsed = stop_time.tv_sec - start_time.tv_sec +
		  ((double) (stop_time.tv_usec - start_time.tv_usec) / 1000000);

	ctrl->rx = (uint64_t) ctrl->count * (uint64_t) ctrl->size;
	ctrl->rx_sum += ctrl->rx;
	ctrl->rx_time = elapsed;
	ctrl->rx_time_sum += elapsed;

	show_stats(ctrl);
	ret = 0;

err_free:
	free(buff);
	if (template)
		free(template);

	return ret;
}

/*
 * Base.
 */

static int handle_client(int server_sock, struct netsum_ctrl *ctrl)
{
	int sock, ret;
	struct sockaddr_in sin;
	socklen_t addrlen = sizeof(sin);
	struct netsum_ctrl_msg ctrl_msg;

	sock = accept(server_sock, (struct sockaddr *) &sin, &addrlen);
	if (sock == -1) {
		fprintf(stderr, "accept() failed: %s\n", strerror(errno));
		return -1;
	}
	fprintf(stdout, "\nClient %s:%d\n",
		inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));

	ctrl->rx = 0;
	ctrl->rx_sum = 0;
	ctrl->rx_time = 0;
	ctrl->rx_time_sum = 0;
	ctrl->tx = 0;
	ctrl->tx_sum = 0;
	ctrl->tx_time = 0;
	ctrl->tx_time_sum = 0;

	while (1) {
		ret = recv_ctrl_message(sock, &ctrl_msg, NULL, NULL);
		if (ret)
			break;

		ctrl->count = ntohl(ctrl_msg.count);
		ctrl->size = ntohl(ctrl_msg.size);
		ctrl->timeout = ntohl(ctrl_msg.timeout);

		if (ctrl_msg.dir)
			ret = send_data_messages(sock, ctrl, NULL, 0);
		else
			ret = recv_data_messages(sock, ctrl, NULL, NULL);
		if (ret)
			break;
	};

	close(sock);

	return ret;
}

static int run_server(struct sockaddr_in *sin, struct netsum_ctrl *ctrl)
{
	int sock;

	sock = open_server_sock(sin, ctrl->timeout, ctrl->size);
	if (sock == -1)
		return -1;

	while(1) { handle_client(sock, ctrl); };

	close(sock);
	return -1;
}

static int run_client(struct sockaddr_in *sin, struct netsum_ctrl *ctrl)
{
	int sock, ret;
	struct netsum_ctrl_msg ctrl_msg;

	sock = connect_server(sin, ctrl->timeout, ctrl->size);
	if (sock == -1)
		return -1;

	ctrl->rx = 0;
	ctrl->rx_sum = 0;
	ctrl->rx_time = 0;
	ctrl->rx_time_sum = 0;
	ctrl->tx = 0;
	ctrl->tx_sum = 0;
	ctrl->tx_time = 0;
	ctrl->tx_time_sum = 0;

	ctrl_msg.count = htonl(ctrl->count);
	ctrl_msg.size = htonl(ctrl->size);
	ctrl_msg.timeout = htonl(ctrl->timeout);

	while (1) {
		if (ctrl->flags & DIR_TX) {
			ctrl_msg.dir = 0;
			ret = send_ctrl_message(sock, &ctrl_msg, NULL, 0);
			if (ret)
				break;
			ret = send_data_messages(sock, ctrl, NULL, 0);
			if (ret)
				break;
		}
		if (ctrl->flags & DIR_RX) {
			ctrl_msg.dir = 1;
			ret = send_ctrl_message(sock, &ctrl_msg, NULL, 0);
			if (ret)
				break;
			ret = recv_data_messages(sock, ctrl, NULL, NULL);
			if (ret)
				break;
		}
	};

	close(sock);

	return ret;
}

int run_netsum(char *addr, struct netsum_ctrl *ctrl)
{
	struct sockaddr_in sin;
	int ret;

	memset(&sin, 0, sizeof(sin));

	if (addr) {
		if (get_inet_addr(addr, &sin)) {
			fprintf(stderr,
				"Failed to convert %s into an IP address\n",
				addr);
			return -1;
		}
		ret = addr_is_local(&sin);
		if (ret < 0)
			return ret;
		if (!ret)
			return run_client(&sin, ctrl);
	} else {
		sin.sin_family = AF_INET;
		sin.sin_port = htons(NETSUM_DEFAULT_PORT);
		sin.sin_addr.s_addr = INADDR_ANY;
	}

	return run_server(&sin, ctrl);
}
