#ifndef BYTEORDER_H
#define BYTEORDER_H

#define __swab32(x) ((uint32_t)(                                      \
        (((uint32_t)(x) & (uint32_t)0x000000ffUL) << 24) |            \
        (((uint32_t)(x) & (uint32_t)0x0000ff00UL) <<  8) |            \
        (((uint32_t)(x) & (uint32_t)0x00ff0000UL) >>  8) |            \
        (((uint32_t)(x) & (uint32_t)0xff000000UL) >> 24)))

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define __LITTLE_ENDIAN 1234
#define __constant_cpu_to_le32(x) ((uint32_t)(x))
#define __constant_cpu_to_be32(x) (__swab32((x)))
#define __cpu_to_le32(x) ((uint32_t)(x))
#define __le32_to_cpu(x) ((uint32_t)(x))
#define __cpu_to_be32(x) (__swab32((x)))
#define __be32_to_cpu(x) __swab32((uint32_t)(x))
#else
#define __BIG_ENDIAN 4321
#define __constant_cpu_to_le32(x) (__swab32((x)))
#define __constant_cpu_to_be32(x) ((uint32_t)(x))
#define __cpu_to_le32(x) (__swab32((x)))
#define __le32_to_cpu(x) __swab32((uint32_t)(x))
#define __cpu_to_be32(x) ((uint32_t)(x))
#define __be32_to_cpu(x) ((uint32_t)(x))
#endif

#if CRC_LE_BITS > 8
# define tole(x) ((uint32_t) __constant_cpu_to_le32(x))
#else
# define tole(x) (x)
#endif

#if CRC_BE_BITS > 8
# define tobe(x) ((uint32_t) __constant_cpu_to_be32(x))
#else
# define tobe(x) (x)
#endif

#endif
