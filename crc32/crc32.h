#ifndef CRC32_H
#define CRC32_H

uint32_t crc32_le(uint32_t crc, unsigned char const *p, size_t len);
uint32_t crc32_be(uint32_t crc, unsigned char const *p, size_t len);

uint32_t crc32c_le(uint32_t crc, unsigned char const *p, size_t len);

#endif
