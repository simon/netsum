/*
 * This file is part of netsum.
 *
 * Copyright (C) 2014 Simon Guinot <simon.guinot@sequanux.org>
 *
 * netsum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * netsum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with netsum.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/limits.h>

#include "netsum.h"
#include "crc32/crc32.h"
#include "buffer.h"

int write_buffer_to_file(const char *fname, unsigned char *buff, size_t size)
{
	int fd;
	int retry;
	int ret;
	char fpath[PATH_MAX];
	char *curr_dir;

	curr_dir = get_current_dir_name();
	if (curr_dir == NULL) {
		fprintf(stderr, "get_current_dir_name() %s failed\n",
		strerror(errno));
		return -1;
	}
	snprintf(fpath, sizeof(fpath), "%s/%s", curr_dir, fname);
	free(curr_dir);

	fd = open(fpath, O_CREAT|O_TRUNC|O_WRONLY, 0644);
	if (fd == -1) {
		fprintf(stderr, "open() %s failed: %s\n", fpath,
			strerror(errno));
		return -1;
	}

	retry = 0;
	while (size && (retry < 3)) {
		ret = write(fd, buff, size);
		if (ret == -1) {
			if (errno == EAGAIN || errno == EINTR) {
				retry++;
				continue;
			}
			fprintf(stderr, "write() %s failed: %s\n",
				fpath, strerror(errno));
			return -1;
		}
		buff += ret;
		size -= ret;
	}

	ret = close(fd);
	if (ret == -1) {
		fprintf(stderr, "close() %s failed: %s\n", fpath,
			strerror(errno));
		return -1;
	}

	return 0;
}

void dump_buffer(unsigned char *buff, int size)
{
	int i;

	fprintf(stderr, "Buffer (%d bytes):\n", size);
	for (i = 0; i < size; i++) {
		fprintf(stderr, "%02x", buff[i]);
		if (i % 16 == 15)
			fprintf(stderr, "\n");
		else if (i % 2 == 1)
			fprintf(stderr, " ");
	}
	fprintf(stderr, "\n");
}

/*
 * Fill buffer with random data and append checksum at the end.
 */
void fill_buffer(unsigned char *buff, int size)
{
	int *rdata = (int *) buff;
	int rsize = size - 4;
	uint32_t *crc = (uint32_t *) &buff[rsize];
	int i;

	for (i = 0; i < rsize / 4; i++)
		rdata[i] = rand();

	*crc = crc32_le(0, buff, rsize);
}

/*
 * Check CRC for the given buffer.
 */
bool buffer_is_valid(unsigned char *buff, int size, bool verbose)
{
	int rsize = size - 4;
	uint32_t *crc_buff = (uint32_t *) &buff[rsize];
	uint32_t crc;

	crc = crc32_le(0, buff, rsize);

	if (verbose && *crc_buff != crc) {
		fprintf(stderr, "%s: computed crc 0x%08x differs from 0x%08x\n",
			__func__, crc, *crc_buff);
		dump_buffer(buff, size);
	}
	return *crc_buff == crc;
}

/*
 * Test functions fill_buffer and check_buffer.
 */
int run_buffer_tests(void)
{
	unsigned char *buff;
	bool crc_match, last_bit_flip;
	int ret = 0;
	int buffer_sizes[] =
		{    4,    99,   100,  101,    128,
		   251,   256,   261,  512,   1024,
		  2048,  4096,  8192, 16384, 32768,
		 65536, MAX_BUFFER_SIZE };  
	int s;

	printf("== Buffer and CRC tests ==\n");

	buff = malloc(MAX_BUFFER_SIZE);
	if (!buff) {
		fprintf(stderr, "malloc() failed: %s\n", strerror(errno));
		return -1;
	}

	crc_match = true;
	last_bit_flip = true;

	for (s = 0; s < ARRAY_SIZE(buffer_sizes); s++) {
		unsigned char *flip;

		fill_buffer(buff, buffer_sizes[s]);
		if (!buffer_is_valid(buff, buffer_sizes[s], false))
			crc_match = false;

		flip = &buff[buffer_sizes[s] - 1];
		*flip = (*flip & 0xfe) | (~*flip & 1);
		if (buffer_is_valid(buff, buffer_sizes[s], false))
			last_bit_flip = false;
	}
	printf("CRC match: %s\n", crc_match ? "OK" : "KO");
	printf("Last bit flip: %s\n", last_bit_flip ? "OK" : "KO");

	if (!crc_match || !last_bit_flip)
		ret = -1;

	free(buff);

	return ret;
}
