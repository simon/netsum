# Build directory (can be overwritten)
O ?= .

# Files.
SRCS = $(wildcard *.c)
OBJS = $(SRCS:%.c=$O/%.o) $O/crc32.o

# Build options.
CC ?= gcc
CFLAGS ?= -Wall -O3
CPPFLAGS ?= -DHAVE_GETOPT_LONG

all: $O/netsum

$O/crc32.o: crc32/crc32.c
	$(CC) -c -DCONFIG_CRC32_SLICEBY8 $(CPPFLAGS) $(CFLAGS) $< -o $@

$O/%.o: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

$O/netsum: $(OBJS)
	$(CC) $(LDFLAGS) $^ -o $@

install: $O/netsum
	@ install -vD -m0755 $< $(DESTDIR)/usr/bin/netsum

uninstall:
	@ rm -vf $(DESTDIR)/usr/bin/netsum

clean:
	@ rm -vf $(OBJS) $O/netsum

.PHONY: all install uninstall clean
