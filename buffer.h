#define MAX_BUFFER_SIZE 262144 /* 256 KB */

int write_buffer_to_file(const char *fname, unsigned char *buff, size_t size);
void dump_buffer(unsigned char *buffer, int size);
void fill_buffer(unsigned char *buffer, int size);
bool buffer_is_valid(unsigned char *buff, int size, bool verbose);
int run_buffer_tests(void);
